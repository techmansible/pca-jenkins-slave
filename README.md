Prudential jenkins-slave installation
----------------------------------

Ansible playbook to Install prerequisties(git, gradle, java)of jenkins-slave, download the binary, untar and install them.

Variables that can be passed dynamically during playbook execution
-------------------------------------------------------------------

run_as                        - User to run the playbook                                                    
jenkins_master_id_rsa         - Use the public key that needs to be copied in jenkins-slave                          
jenkins_slave_username        - Username of jenkins-slave
gradle_binary_main_directory  - Main directory of gradle configuration

Running Playbook Example
------------------------

ansible-playbook -i inventories -e "host_group=node" jenkins-slave-install.yml

Inventory
----------

Update hostnames in inventories/hosts file. Example:

[node]
10.0.0.5 

Roles
-------
Below are the roles used in this playbook

* jenkins-slave-prerequisites -- Installs the pre-requisites(git, gradle, java)of jenkins-slave.

